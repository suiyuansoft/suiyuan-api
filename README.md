持续更新，汇总全网免费API

### 旗下网站
随缘博客：[blog.63ik.cn](http://blog.63ik.cn/)
随缘导航：[link.sybapi.cc](http://link.sybapi.cc)
### 公益项目
free-api：[https://gitee.com/suiyuansoft/free-api](https://gitee.com/suiyuansoft/free-api)

### 随缘API
网址：[https://www.sybapi.cc/](https://www.sybapi.cc/)
交流群：695963494

稳定、快速、免费的 API 接口服务，拒绝流量劫持，全面使用 HTTPS！
![随缘API](https://images.gitee.com/uploads/images/2021/1014/215849_5d3a836e_7967827.png "QQ截图20211014211759.png")


### 阿里云api市场
网址：[https://market.aliyun.com/data](https://market.aliyun.com/data)

这个是付费api市场，提供稳定丰富的api
![阿里云api市场](https://images.gitee.com/uploads/images/2021/1014/220006_5f0af9da_7967827.png "QQ截图20211014212051.png")

总结
如果需要更稳定的api，推荐大家使用付费api

欢迎大家提交免费 api 我们共同维护这篇文章